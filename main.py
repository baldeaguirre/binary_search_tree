import random
from arbol_binario import Nodo

# random.seed(0) # Se genera una semilla para fijar números aleatorios

def main():
    lista = [3, 6, 10, 14, 1, 4, 13, 7]

    arbol = Nodo(8)

    for num in lista:
        arbol.insertar(num)

    lista = [random.randint(1, 20) for i in range(10)]

    for num in lista:
        print(arbol.buscar(num))

    # # Se crea una lista de números aleatorios enteros para crear árbol binario
    # lista = [random.randint(1, 100) for i in range(100)]

    # arbol = Nodo(random.randint(1, 100))
    # print("Nodo raíz: {}".format(arbol.número), '\n')

    # for num in lista:
    #     arbol.insertar(num)

    print('\n', "Recorrido preorden: ")
    arbol.preOrden()
    print('\n', "Recorrido en orden: ")
    arbol.enOrden()
    print('\n', "Recorrido postorden: ")
    arbol.postOrden()

main()
