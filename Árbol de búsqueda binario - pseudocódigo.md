INICIO

Se declara la clase `Nodo `con los atributos:` self.número, self.izquierdo, self.derecho.`

Se declara el método `insertar:`

* Si el valor del atributo `self.número `no es igual a None:
  * Si el valor del número a insertar es menor que el valor del atributo `self.número `y si el valor del atributo `self.izquierdo` es un valor `None:`
    * Insertar valor en el atributo `self.izquierdo` del árbol binario.
  * Si no:
    * Repetir proceso utilizando recursión con el método `insertar.`
  * Si el valor del número a insertar es mayor que el valor del atributo `self.número `y si el valor del atributo `self.derecho` es un valor `None:`
    * Insertar valor en el atributo `self.derecho` del árbol binario.
  * Si no:
    * Repetir proceso utilizando recursión con el método `insertar.`
* Si no:
  * Se inicializa el atributo self.número con el valor a insertar como nodo raíz.

Se declara el método `buscar:`

* Si el valor a buscar es menor que el valor del atributo `self.número:`
  * Si el valor del atributo `self.izquierdo` es un valor `None:`
    * Retornar un string con el mensaje: `"El valor no fué encontrado".`
  * Si no:
    * Repetir proceso utilizando recursión con el método `buscar.`
* Si el valor a buscar es mayor que el valor del atributo `self.número:`
  * Si el valor del atributo `self.derecho` es un valor `None:`
    * Retornar un string con el mensaje: `"El valor no fué encontrado".`
  * Si no:
    * Repetir proceso utilizando recursión con el método `buscar.`
* Si no:
  * Retornar un string con el mensaje: `"El valor fué encontrado".`

Se declara el método `preOrden:`

* Se imprimen los elementos del árbol binario en el siguiente órden:
  * `nodo raíz ---> izquierda ---> derecha`

Se declara el método `enOrden:`

* Se imprimen los elementos del árbol binario en el siguiente órden:
  * `izquierda ---> nodo raíz ---> derecha`

Se declara el método `postOrden:`

* Se imprimen los elementos del árbol binario en el siguiente órden:
  * `izquierda ---> derecha ---> nodo raíz`

Se declara función `main:`

* Se genera una lista de elementos a insertar en el árbol binario:
* ```python
  lista = [3, 6, 10, 14, 1, 4, 13, 7]
  ```
* Se inicializa una instancia de la clase `Nodo` con el valor del nodo raíz:
  * ```python
    arbol = Nodo(8)
    ```
* Se itera con un ciclo `for` para insertar los valores de la lista en el árbol binario:
* ```python
  for num in lista:
          arbol.insertar(num)
  ```
* Se genera una lista de 10 valores enteros aleatorios del 1 al 20 para verificar si los valores se encuentran en el árbol binario:
* ```python
  lista = [random.randint(1, 20) for i in range(10)]
  ```
* Se itera sobre los elementos de la lista de valores aleatorios utilizando el método `buscar `para observar que valores se encuentran en el árbol binario:
* ```python
  for num in lista:
          print(arbol.buscar(num)) 
  ```
* Se imprimen los elementos del árbol binario utilizando los métodos preOrden, enOrden y postOrden.

FIN.