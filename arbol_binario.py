class Nodo:
    def __init__(self, número):
    # Se crean atributos de la clase 'Nodo'
        self.número = número
        self.izquierdo = None
        self.derecho = None
    #
    def insertar(self, número):
    # Se crea método 'insertar' para crear los nodos
        if self.número is not None:
            if número < self.número:
                if self.izquierdo is None:
                    self.izquierdo = Nodo(número)
                else:
                    # Recursión para insertar el nuevo número del lado izquierdo
                    self.izquierdo.insertar(número)
            elif número > self.número:
                if self.derecho is None:
                    self.derecho = Nodo(número)
                else:
                    # Recursión para insertar el nuevo número del lado derecho
                    self.derecho.insertar(número)
        else:
            self.número = número
    #
    def buscar(self, valor):
    # Se crea método 'buscar' para comparar el valor con los valores de los nodos
        if valor < self.número:
            if self.izquierdo is None:
                return str(valor)+" no fué encontrado."
            # Recursión para buscar el valor en la subrama izquierda del árbol
            return self.izquierdo.buscar(valor)        
        elif valor > self.número:
            if self.derecho is None:
                return str(valor)+" no fué encontrado."
                # Recursión para buscar el valor en la subrama derecha del árbol
            return self.derecho.buscar(valor)        
        else:
            return str(self.número)+" fué encontrado."
    #
    def preOrden(self):
    # Se crea método 'preOrden' para imprimir árbol binario pre-ordenado
    # nodo raíz -> izquierda -> derecha
        if self:
            print(self.número)
            if self.izquierdo:
                self.izquierdo.preOrden()
            if self.derecho:
                self.derecho.preOrden()
    #
    def enOrden(self):
    # Se crea método 'enOrden' para imprimir árbol binario ordenado
    # izquierda -> nodo raíz -> derecha
        if self:
            if self.izquierdo:
                self.izquierdo.enOrden()
            print(self.número)
            if self.derecho:
                self.derecho.enOrden()
    #
    def postOrden(self):
    # Se crea método 'postOrden' para imprimir árbol binario post-ordenado
    # izquierda -> derecha -> nodo raíz
        if self:
            if self.izquierdo:
                self.izquierdo.postOrden()
            if self.derecho:
                self.derecho.postOrden()
            print(self.número)
